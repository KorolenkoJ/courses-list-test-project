import Vue from 'vue';
import VueRouter from 'vue-router';
import Courses from '../components/coursesList.vue';
import Course from '../components/course.vue';
import CourseEdit from '../components/course_edit.vue';
import Students from '../components/students.vue';
import Student from '../components/student.vue';
import StudentEdit from '../components/student_edit.vue';

const Home = { template: '<p>главная</p>' }

Vue.use(VueRouter);

export const router = new VueRouter({
    routes: [    
        {    
            path: '/',    
            name: 'courses',    
            component: Courses    
          }, 
        {    
          path: '/add_course',    
          name: 'add_course',    
          component: Course    
        },
        {    
          path: '/edit_course/:id',    
          name: 'edit_course',    
          props: true,
          component: CourseEdit,
        },
        {
          path: '/students/:id',
          name: 'students',
          props: true,
          component: Students,
        },
        {
          path: '/add_student',
          name: 'add_student',
          props: true,
          component: Student,
        },
        {
          path: '/edit_student/:idx/:id',
          name: 'edit_student',
          props: true,
          component: StudentEdit,
        }
      ]
});