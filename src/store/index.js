import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        courses: [],
        
    },
    mutations: {
        UPDATE_COURSE_STATE(state, data){
            return state.courses = [...data];
        },

        ADD_ITEM(state, data) {            
            if ('key' in data) {
                state.courses[data.id][data.key].push(data.data);
            }
            else {
                state.courses.push(data);
            }
        },
        
        REMOVE_ITEM(state, data) {
            if ('key' in data) {
                state.courses[data.idxCourse][data.key].splice(data.idxStudents);
            }
            else {
                state.courses.splice(data.id);
            }
        },

        EDIT_ITEM(state, data) {
            if('key' in data) {
                state.courses[data.idxCourse][data.key][data.idxStudents].name = data.name;
                state.courses[data.idxCourse][data.key][data.idxStudents].email = data.email;
                state.courses[data.idxCourse][data.key][data.idxStudents].status = data.status;
            }
            else {
                state.courses[data.id].name = data.name;
                state.courses[data.id].code = data.code;
            }
        },
    },
    getters: {
        courses(state) {
            return state.courses;
        },
        getCurIdx: (state) => (id) => {
            return state.courses.findIndex((el) => {
                return el.id === id;
            });
        }

    },
    actions: {
        updateCourseState({commit}, data) {
            commit('UPDATE_COURSE_STATE', data);
        },
        addItem({commit}, obj) {
            commit('ADD_ITEM', obj)
        },
        removeItem({commit}, id) {
            commit('REMOVE_ITEM', id);
        },
        editItem({commit}, data) {
            commit('EDIT_ITEM', data);
        },
    },
});